autocmd BufNewFile,BufRead known_hosts		setfiletype known_hosts
autocmd BufNewFile,BufRead */.ssh/known_hosts.*	setfiletype known_hosts
