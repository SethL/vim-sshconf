" Vim syntax for ssh authorized_keys files

if exists("b:current_syntax") | finish | endif

" fields: options (opt), keytype, b64-key, comment

syntax match ak_CmLine	/^#.*$/



" FIXME -- this is lazy.  opts can have spaces when quoted
syntax match ak_Options /\S\+/			contains=ak_Opt,ak_OptSep
						\ nextgroup=ak_KeyType skipwhite
syntax match ak_Opt	/[^,]/			contained
						\ nextgroup=ak_OptSep
syntax match ak_OptSep	/,/			contained
						\ nextgroup=ak_KeyType



syntax match ak_KeyType /\v(ssh|ecdsa)-\S+/	
						\ nextgroup=ak_Key skipwhite

syntax match ak_Key	/\S\+/			contained
						\ nextgroup=ak_Comment skipwhite

syntax match ak_Comment	/.*$/			contained


hi def link ak_Comment		Comment
hi def link ak_CmLine		Comment
hi def link ak_Opt		Identifier
hi def link ak_OptSep		Delimiter
hi def link ak_KeyType		Type
hi def link ak_Key		Normal

let b:current_syntax = "authorized_keys"
