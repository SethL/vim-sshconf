" Vim syntax for ssh known_hosts files

if exists("b:current_syntax") | finish | endif

" fields: markers (opt), hostnames, keytype, b64-key, comment

syntax match kh_CmLine	/^#.*$/

syntax match kh_Begin	/^/			nextgroup=kh_Hosts

syntax match kh_Markers	/^@\w\+/		nextgroup=kh_Hosts skipwhite

syntax match kh_Hosts	/\S\+/			contained
						\ contains=kh_Host,kh_HostSep
						\ nextgroup=kh_KeyType skipwhite

syntax match kh_Host	/[^,]/			contained
						\ nextgroup=kh_HostSep

syntax match kh_HostSep	/,/			contained
						\ nextgroup=kh_Host

syntax match kh_KeyType	/\v(ssh|ecdsa)-\S+/	contained
						\ nextgroup=kh_Key skipwhite

syntax match kh_Key	/\S\+/			contained
						\ nextgroup=kh_Comment skipwhite

syntax match kh_Comment	/.*$/			contained


hi def link kh_Comment		Comment
hi def link kh_CmLine		Comment
hi def link kh_Markers		Todo
hi def link kh_Hosts		Transparent
hi def link kh_Host		Identifier
hi def link kh_HostSep		Delimiter
hi def link kh_KeyType		Type
hi def link kh_Key		Normal

let b:current_syntax = "known_hosts"
